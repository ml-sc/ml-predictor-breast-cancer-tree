from predict import predict


def test_predict0():
    data = {'data': {
        'values': [19.02, 24.59, 122.0, 1076.0, 0.09029, 0.1206, 0.1468, 0.08271, 0.1953, 0.05629, 0.5495, 0.6636,
                   3.055, 57.65, 0.003872, 0.01842, 0.0371, 0.012, 0.01964, 0.003337, 24.56, 30.41, 152.9, 1623.0,
                   0.1249, 0.3206, 0.5755, 0.1956, 0.3956, 0.09288
                   ]}}
    assert predict(data).get('result', {}).get('prediction') == '[0]'


def test_predict1():
    data = {'data': {
        'values': [12.36, 21.8, 79.78, 466.1, 0.08772, 0.09445, 0.06015, 0.03745, 0.193, 0.06404, 0.2978, 1.502, 2.203,
                   20.95, 0.007112, 0.02493, 0.02703, 0.01293, 0.01958, 0.004463, 13.83, 30.5, 91.46, 574.7, 0.1304,
                   0.2463, 0.2434, 0.1205, 0.2972, 0.09261
                   ]}}
    assert predict(data).get('result', {}).get('prediction') == '[1]'
