import json
import os
import pathlib

from kafka import KafkaConsumer, KafkaProducer

from predict import predict

with open(pathlib.Path(__file__).parent / 'data' / 'name') as f:
    name = f.read().strip()

bootstrap_servers = os.environ.get('KAFKA_BOOTSTRAP_SERVERS', 'localhost:9096')
data_topic = os.environ.get('KAFKA_DATA_TOPIC', name)
results_topic = os.environ.get('KAFKA_RESULTS_TOPIC', 'ml-prediction-result')

producer = KafkaProducer(bootstrap_servers=bootstrap_servers,
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))
consumer = KafkaConsumer(data_topic,
                         bootstrap_servers=bootstrap_servers,
                         auto_offset_reset='earliest',
                         group_id=data_topic,
                         value_deserializer=lambda x: json.loads(x.decode('utf-8')))

print(f'Started to listen on: {data_topic}')
for record in consumer:
    data = json.loads(record.value)
    print(f'Got data: {data}')
    result = predict(data)
    print(f'Predicted result: {result}')
    producer.send(results_topic, result)
    producer.flush()
