import pathlib

import joblib
from sklearn import tree
from sklearn.datasets import load_breast_cancer

X, y = load_breast_cancer(return_X_y=True)
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, y)
joblib.dump(clf, pathlib.Path(__file__).parent / 'data' / 'model.pkl', compress=9)


def p(x):
    return ','.join(str(f) for f in x)


print(p(X[87]))
print(p(X[88]))
